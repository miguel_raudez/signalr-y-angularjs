﻿using SignalRAngular.Models;
using SignalRAngular.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SignalRAngular.Controllers
{
    public class MonitorVentaController : ApiController
    {
        private MonitorContext db = new MonitorContext();

        [HttpGet]
        public IEnumerable<MonitorVentaCampanaVM> MonitorVentaCampana(int id)
        {
            return db.Database.SqlQuery<MonitorVentaCampanaVM>("EXEC [dbo].[MonitorVentaCampana] @CampanaId", new SqlParameter("CampanaId", id)).ToList();
        }
    }
}
