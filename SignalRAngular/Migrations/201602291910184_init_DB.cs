namespace SignalRAngular.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init_DB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Ventas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ColaboradorId = c.Int(nullable: false),
                        Verificada = c.Int(nullable: false),
                        Monto = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Activo = c.Boolean(nullable: false),
                        Creado = c.DateTime(nullable: false, defaultValueSql: "GETDATE()"),
                        Actualizado = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Ventas");
            DropTable("dbo.UserProfile");
        }
    }
}
