﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SignalRAngular.Models
{
    public class Venta: Base
    {
        [Key]
        public int Id { get; set; }

        public int ColaboradorId { get; set; }

        public int Verificada { get; set; }

        public decimal Monto { get; set; }
    }
}