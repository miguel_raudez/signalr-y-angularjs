﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalRAngular.Models
{
    public class Base
    {
        public bool Activo { get; set; }

        public DateTime Creado { get; set; }
        public DateTime? Actualizado { get; set; }
    }
}