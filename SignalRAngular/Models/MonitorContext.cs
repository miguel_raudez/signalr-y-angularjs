﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SignalRAngular.Models
{
    public class MonitorContext : DbContext
    {
        public MonitorContext()
            : base("DefaultConnection")
        {
        }

        public DbSet<UserProfile> UserProfiles { get; set; }
        //public DbSet<TodoItem> TodoItems { get; set; }
        //public DbSet<TodoList> TodoLists { get; set; }
        public DbSet<Venta> Venta { get; set; }
    }
}