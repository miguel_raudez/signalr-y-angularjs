﻿angular.module('App', ['ngTable'])
        .controller("MonitorController", ['$scope', '$http', 'NgTableParams',
        function ($scope, $http, ngTableParams) {
            $scope.loagin = true;

            $scope.options = {
                reactive: true,
                //width: (int)(optional) desired max width in pixels
                //height: (int)(optional) desired height in pixels
                //backgroundcolor: (string)(optional)(default: #fffff) table header bg color
            };

            //$scope.tableParams = new ngTableParams({
            //    page: 1,            // show first page
            //    count: 10000           // count per page
            //}, {
            //    counts: [],
            //    getData: function ($defer, params) {
            //        $defer.resolve(data);
            //    }
            //});
            

            $scope.campanas = [
                {
                    nombre: "Movistar",
                    id: 6,
                    img: "/Images/movistar.png"
                },
                {
                    nombre: "Ficohsa",
                    id: 4,
                    img: "/Images/ficohsa.png"
                },
                {
                    nombre: "Focus",
                    id: 10,
                    img: "/Images/focus.png"
                },
            ]

            $http.get('/api/MonitorVenta/4')
                .then(
                function (result) {
                    //$scope.tableParams = new NgTableParams({ count: 100 }, { dataset: result.data });
                    $scope.tableParams = new ngTableParams({
                        page: 1,            // show first page
                        count: 10000           // count per page
                    }, {
                        counts: [],
                        getData: function ($defer, params) {
                            $defer.resolve(result.data);
                        }
                    });

                    console.log(result);
                    $scope.loagin = false;
                },
                function (result) {
                    console.log(result);
                    $scope.loagin = false;
                });
        }])
        .directive('fixedTableHeaders', ['$timeout', function ($timeout) {
            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    $timeout(function () {
                        container = $(element).parentsUntil(attrs.fixedTableHeaders);
                        $(element).stickyTableHeaders({ scrollableArea: container, "fixedOffset": 2 });

                    }, 0);
                }
            }
        }]);